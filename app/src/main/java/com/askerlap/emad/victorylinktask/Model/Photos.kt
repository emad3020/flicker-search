package com.askerlap.emad.victorylinktask.Model

import java.io.Serializable

data class Photos(
    val page:Int ,
    val pages : Int ,
    val perpage: Int ,
    val total : String,
    val photo: List<Photo>

    ) : Serializable