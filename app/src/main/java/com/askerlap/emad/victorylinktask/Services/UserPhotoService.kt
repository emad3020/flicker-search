package com.askerlap.emad.victorylinktask.Services

import com.askerlap.emad.victorylinktask.Model.Photo
import com.askerlap.emad.victorylinktask.Model.SearchResponse
import com.askerlap.emad.victorylinktask.Networking.FlicerApiService
import com.askerlap.emad.victorylinktask.Utilites.APIKEY
import com.askerlap.emad.victorylinktask.Utilites.QUERY_SEARCH
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object UserPhotoService {

    var mApi: FlicerApiService? = null
    var selectedUserPhotos  = ArrayList<Photo>()

    fun getUserPhotos(userId : String, pageNumber: Int,  completion: (Boolean) -> Unit){

        mApi  = FlicerApiService.create()

        if (pageNumber == 1) {
            selectedUserPhotos.clear()
        }

        mApi!!.getPhotos(QUERY_SEARCH, APIKEY,userId,pageNumber).enqueue(object : Callback<SearchResponse> {

            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
                selectedUserPhotos.addAll(response.body()?.photos!!.photo)

                completion(true)
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {

                completion(false)
            }
        })

    }
}