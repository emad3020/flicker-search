package com.askerlap.emad.victorylinktask.controller

import android.os.Bundle
import androidx.core.app.NavUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import com.askerlap.emad.victorylinktask.Adapter.UserPhotoAdapter
import com.askerlap.emad.victorylinktask.Model.Photo
import com.askerlap.emad.victorylinktask.R
import com.askerlap.emad.victorylinktask.Services.UserPhotoService
import com.askerlap.emad.victorylinktask.Utilites.InfiniteScrollListener
import com.askerlap.emad.victorylinktask.Utilites.SHARED_USER_ID

class UserPhotosActivity : AppCompatActivity() {

    private lateinit var mUserPhotoRV : RecyclerView
    var layoutManager : LinearLayoutManager? = null
    var adapter : UserPhotoAdapter? = null
    var initialPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_photos)

        mUserPhotoRV = findViewById(R.id.userPhotoRV)
        layoutManager = LinearLayoutManager(this)
        mUserPhotoRV.setHasFixedSize(true)
        mUserPhotoRV.layoutManager = layoutManager

        val sharedUser = intent.getStringExtra(SHARED_USER_ID)
        supportActionBar?.title = "More Photos from this user"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = UserPhotoAdapter(this,ArrayList<Photo>())
        mUserPhotoRV.adapter = adapter

        loadUserPhotos(sharedUser,initialPage)

        mUserPhotoRV.addOnScrollListener(InfiniteScrollListener({
            initialPage += 1
            Log.e("TGD PROFILE---->","$initialPage")
            loadUserPhotos(sharedUser,initialPage)
        },layoutManager!!))

    }



    fun loadUserPhotos(userId : String , pageNumber : Int) {
        UserPhotoService.getUserPhotos(userId,pageNumber){success->
            if (success) {
                adapter!!.loadMoreItems(UserPhotoService.selectedUserPhotos)
                adapter!!.notifyDataSetChanged()

            } else {

            }
        }
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
