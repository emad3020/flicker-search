package com.askerlap.emad.victorylinktask.controller

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import com.askerlap.emad.victorylinktask.Adapter.SearchAdapter
import com.askerlap.emad.victorylinktask.R
import com.askerlap.emad.victorylinktask.Services.SearchService
import com.askerlap.emad.victorylinktask.Utilites.InfiniteScrollListener
import com.askerlap.emad.victorylinktask.Utilites.SHARED_USER_ID

class MainActivity : AppCompatActivity() {

    lateinit var resultRecycler : RecyclerView
    lateinit var searchTxt : EditText
    var adapter: SearchAdapter?= null
    var layoutManager : LinearLayoutManager? = null
    var initialPage = 1
    var searchKey : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Flicker Photo Search"

        resultRecycler = findViewById(R.id.resultRV)
        searchTxt = findViewById(R.id.keywordTxt)

        layoutManager = LinearLayoutManager(this)
        resultRecycler.setHasFixedSize(true)
        resultRecycler.layoutManager = layoutManager

        searchTxt.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {

                    searchKey = searchTxt.text.toString()
                    if (searchKey == null || searchKey!!.isEmpty()) {
                        searchTxt.error = "type any key first"

                    } else {
                        performSearch(searchKey!!, initialPage)
                    }


                    true
                }
                else -> false
            }
        }

        resultRecycler.addOnScrollListener(InfiniteScrollListener({

            if (searchKey != null) {
                initialPage += 1

                loadMore(searchKey!!, initialPage)
            }


        }, layoutManager!!))

    }

    fun performSearch(keyword: String, index : Int) {
        SearchService.startSearch(keyword,index){ success ->
            if (success) {

                adapter = SearchAdapter(this, SearchService.photoArray){ itemPosition ->
                    val userIntent = Intent(this,UserPhotosActivity::class.java)
                    userIntent.putExtra(SHARED_USER_ID,SearchService.photoArray[itemPosition].owner)
                    startActivity(userIntent)


                }
                resultRecycler.adapter = adapter!!



            } else {
                Toast.makeText(this,"No Result Found", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun loadMore(keyword: String,index : Int){

        SearchService.startSearch(keyword,index){ success ->
            if (success) {
                adapter!!.refreshItem(SearchService.photoArray)

            }
        }
    }
}
