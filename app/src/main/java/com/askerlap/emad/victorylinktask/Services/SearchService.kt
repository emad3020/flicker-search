package com.askerlap.emad.victorylinktask.Services

import android.util.Log
import com.askerlap.emad.victorylinktask.Model.Photo
import com.askerlap.emad.victorylinktask.Model.SearchResponse
import com.askerlap.emad.victorylinktask.Networking.FlicerApiService
import com.askerlap.emad.victorylinktask.Utilites.APIKEY
import com.askerlap.emad.victorylinktask.Utilites.QUERY_SEARCH
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object SearchService {

    var mApi: FlicerApiService? = null
    var photoArray = ArrayList<Photo>()

    fun startSearch(keyword : String, pageNumer : Int,completion : (Boolean) -> Unit){

        mApi = FlicerApiService.create()

        if (pageNumer == 1 ) {
            photoArray.clear()
        }

        Log.e("TAG---->" , "$pageNumer")

        mApi!!.searchByTag(QUERY_SEARCH,APIKEY,keyword,pageNumer).enqueue(object : Callback<SearchResponse> {

            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {

                if (response.body() != null) {
                    photoArray.addAll(response.body()!!.photos.photo as ArrayList<Photo>)
                }


                completion(true)
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {

                completion(false)
            }
        })

    }
}