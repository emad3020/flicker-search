package com.askerlap.emad.victorylinktask.Utilites


/*
https://api.flickr.com/services/rest/?method=flickr.photos.search
// &api_key=eacd564e9e3d78015215d2864bd9d743&tags=camera&format=json&nojsoncallback=1
// &auth_token=72157704624932245-4b9d797eb4df05d9&api_sig=d3de93a54cdc7fa26fdd20407adbb1ee

*/

const val APIKEY = "2edf37b890ea8a24298bb787f3414247"
const val BASE_URL = "https://api.flickr.com/"
const val QUERY_SEARCH = "flickr.Photos.search"
const val QUERY_PHOTO = "flickr.people.getPhotos"
const val SHARED_USER_ID = "sharedUser"
