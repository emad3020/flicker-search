package com.askerlap.emad.victorylinktask.Networking

import com.askerlap.emad.victorylinktask.Model.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FlicerApiService {


    @GET("services/rest/")
    fun searchByTag(@Query("method") method : String,
                    @Query("api_key") apiKey : String,
                    @Query("text") searchTag: String,
                    @Query("page") pageIndex : Int,
                    @Query("format") responseFormat: String ="json",
                    @Query("nojsoncallback") jsonCalls : Int = 1) : Call<SearchResponse>


    /*
    *
    *
    * **/

    @GET("services/rest/")
    fun getPhotos(@Query("method") method : String,
                    @Query("api_key") apiKey : String,
                    @Query("user_id") userId: String,
                    @Query("page") pageIndex : Int,
                    @Query("format") responseFormat: String ="json",
                    @Query("nojsoncallback") jsonCalls : Int = 1) : Call<SearchResponse>


    companion object Factory{

        fun create(): FlicerApiService {
            return ApiClient.getClient()!!.create(FlicerApiService::class.java)
        }

    }
}