package com.askerlap.emad.victorylinktask.Networking

import com.askerlap.emad.victorylinktask.Utilites.BASE_URL
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    var retrofit : Retrofit? = null



    fun getClient() : Retrofit? {

        if (retrofit == null) {


            val gson = GsonBuilder().setLenient().create()
            retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)

                .build()
        }

        return retrofit
    }
}