package com.askerlap.emad.victorylinktask.Model

import java.io.Serializable

data class  SearchResponse (
    val photos: Photos,
    val stat : String

): Serializable