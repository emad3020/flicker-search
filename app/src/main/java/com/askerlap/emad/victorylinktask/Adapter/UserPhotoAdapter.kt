package com.askerlap.emad.victorylinktask.Adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.askerlap.emad.victorylinktask.Model.Photo
import com.askerlap.emad.victorylinktask.R
import com.squareup.picasso.Picasso

class UserPhotoAdapter(val context: Context, var userPhotos : List<Photo> ) : RecyclerView.Adapter<UserPhotoAdapter.photoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, ViewType: Int): photoHolder {
        val rootView = LayoutInflater.from(context)
            .inflate(R.layout.row_item_layout,parent,false)

        return photoHolder(rootView)

    }

    override fun getItemCount(): Int {
        return userPhotos.count()
    }

    override fun onBindViewHolder(holder: photoHolder, position: Int) {

        val item = userPhotos[position]
        holder.bindView(item)

    }



    inner class photoHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        val img = itemView.findViewById<ImageView>(R.id.searchImg)
        val mTitle = itemView.findViewById<TextView>(R.id.titleTxt)

        fun bindView(photo : Photo) {
            mTitle.text = photo.title
            val photoUrl = "https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg"
            img.loadFromUrl(photoUrl)


        }
    }

    fun loadMoreItems(newItems : List<Photo>){
        userPhotos = newItems
        notifyDataSetChanged()
    }
}


private fun ImageView.loadFromUrl(url : String) {
    Picasso.get()
        .load(url)
        .into(this)
}