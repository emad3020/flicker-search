package com.askerlap.emad.victorylinktask.Adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.askerlap.emad.victorylinktask.Model.Photo
import com.askerlap.emad.victorylinktask.R
import com.squareup.picasso.Picasso

class SearchAdapter (val context : Context, var results : List<Photo>, val itemClicked : (Int) -> Unit )
    : RecyclerView.Adapter<SearchAdapter.searchHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): searchHolder {

        val rootView = LayoutInflater.from(context)
            .inflate(R.layout.row_item_layout,parent,false)
        return searchHolder(rootView,itemClicked)
    }

    override fun getItemCount(): Int {

        return  results.count()
    }

    override fun onBindViewHolder(holder: searchHolder, position: Int) {

        val photoItem = results[position]
        holder.bindView(photoItem)


    }

    inner class searchHolder(itemView : View, val itemClicked: (Int) -> Unit ) : RecyclerView.ViewHolder(itemView) {

        val mTitleTxt = itemView.findViewById<TextView>(R.id.titleTxt)
        val mSearchImg = itemView.findViewById<ImageView>(R.id.searchImg)

        fun bindView(photo : Photo){

            mTitleTxt.text = photo.title
            //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
            val photoUrl = "https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg"
            mSearchImg.loadImageUrl(photoUrl)
            itemView.setOnClickListener { itemClicked(adapterPosition) }

        }
    }


    fun refreshItem(newArray : List<Photo>){
        results = newArray
        notifyDataSetChanged()
    }
}


private fun ImageView.loadImageUrl(url : String){
    Picasso
        .get()
        .load(url)
        .into(this)
}